from u_a_api import models, create_app

app = create_app()

with app.app_context():
    models.db.init_app(app)
    models.db.create_all()
