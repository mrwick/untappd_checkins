from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Creds(db.Model):
    client_id = db.Column(
            db.String(40),
            nullable=False,
            primary_key=True)
    client_secret = db.Column(
            db.String(40),
            nullable=False)

class Users(db.Model):
    user_id = db.Column(
            db.Integer,
            primary_key=True)
    login =  db.Column(
            db.String(80),
            nullable=False)
    password =  db.Column(
            db.String(80),
            nullable=False)
    mail =  db.Column(
            db.String(250))
    access_token = db.Column(
            db.String(40))
    admin = db.Column(
            db.Boolean,
            nullable=False,
            default=False)
    children = db.relationship(
            "Checkins",
            backref='user',
            cascade='all, delete-orphan',
            lazy=True)

class Checkins(db.Model):
    checkin_id = db.Column(
            db.Integer,
            nullable=False,
            primary_key=True)
    user_id = db.Column(
            db.Integer,
            db.ForeignKey('users.user_id'),
            nullable=False)
    date = db.Column(db.String)
    name = db.Column(db.String)
    brewery = db.Column(db.String)
    place = db.Column(db.String)
    comments = db.Column(db.String)
