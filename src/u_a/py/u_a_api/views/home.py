from flask import Blueprint, render_template, g
from ..models import Creds

bp = Blueprint('home', __name__)

@bp.route('/')
def index():
    allcreds = Creds.query.all()
    if len(allcreds) < 1:
        return "no creds"
    g.client_id = allcreds[0].client_id
    return render_template('index.html')
