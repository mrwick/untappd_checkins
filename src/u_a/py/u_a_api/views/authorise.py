import requests
from flask import (
        Blueprint,
        g,
        render_template,
        url_for,
        redirect,
        )
from ..models import Creds

ok = requests.codes.ok
bp = Blueprint('authorise', __name__)

@bp.route('/authorise/<code>')
def authorise(code):
    allcreds = Creds.query.all()
    if len(allcreds) < 1:
        return "no creds"
    return {
            'meta': {'http_code': 200},
            'response': {'access_token': 'token'},
            'code': code
            }

    fsu = 'freeshell.de/~mrwick/untappd_checkins'
    headers = {
            'user-agent':
            'bierwick ({})'.format(creds[0].client_id)}
    auts_url = 'https://untappd.com/oauth/authorize'
    auts_url_paarms = {
            'client_id': creds[0].client_id,
            'client_secret': 'secret',
            'response_type': 'code',
            'redirect_url': fsu,
            'code=': code,
            }
    auts_req = requests.get(
            auts_url,
            params=auts_url_params,
            headers=headers)
    if auts_req.status_code != ok:
        auts_req.raise_for_status()
