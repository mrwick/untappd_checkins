# add user

import requests
from flask import (
        Blueprint,
        g,
        render_template,
        url_for,
        redirect,
        )
from ..models import Users

ok = requests.codes.ok
bp = Blueprint('useradd', __name__)

@bp.route('/useradd/<login>/<password>/<mail>')
def useradd(login, passwrod, mail):
    existe = Users.query.filter_by(login=login).all()
    if existe:
        return {user: 'False'}
    user = Users(
            login=login,
            password=password,
            e_mail=mail)
    return {user: 'True'}
