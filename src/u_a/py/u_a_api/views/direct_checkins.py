import requests
from flask import Blueprint, render_template
from ..models import Creds

ok = requests.codes.ok
bp = Blueprint('direct_checkins', __name__)

@bp.route('/direct_checkins')
def direct_checkins():
    return {'not implemented': "user login implementation necessary"}

    creds = Creds.query.all()
    if len(creds) < 1:
        return "no creds"

    headers = {
            'user-agent':
            'bierwick ({})'.format(creds[0].client_id)}
    direct_url = 'https://api.untappd.com/v4/user/checkins'
    direct_url_params = {'access_token': creds[0].access_token}
    direct_req = requests.get(
            direct_url,
            params=direct_url_params,
            headers=headers)
    if direct_req.status_code == ok:
        return direct_req.json()
    direct_req.raise_for_status()
