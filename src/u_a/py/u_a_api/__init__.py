import os

from flask import Flask, g
from flask_cors import CORS
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy

from .models import db, Creds
from .views import (
        home,
        authenticate,
        authorise,
        admin,
        direct_checkins,
        user,
        useradd,
        )

instance_config = 'dev_config.py'
from . import dev_config as configuration

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(configuration)
    app.config.from_pyfile(instance_config)

    db_name = 'u_a_api.sqlite'
    db_path = os.path.join(app.instance_path, db_name)
    db_uri = 'sqlite:///{}'.format(db_path)
    app.config.from_mapping(SQLALCHEMY_DATABASE_URI=db_uri)
    
    CORS(app)
    api = Api(app)
    db.init_app(app)

    app.register_blueprint(home.bp)
    app.register_blueprint(authenticate.bp)
    app.register_blueprint(authorise.bp)
    app.register_blueprint(admin.bp)
    app.register_blueprint(direct_checkins.bp)
    app.register_blueprint(user.bp)
    app.register_blueprint(useradd.bp)
  
    return app

if __name__ == '__main__':
    create_app()
