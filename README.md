# condensed_check-ins
---

## description
Very simple read-only list of untappd check-ins.


Every entry shall contain only:

 * Date
 * Beer
 * Brewery
 * Place

## prerequisites

 * flask
 * flask-restful
 * flask-cors
 * requests

## backend

Flask-RESTful creates all the needed connections to the untappd api.

## frontend

javascript.fetch calls to localhost interacts with the backend
 * this, of course, does not work because javascript is on the client and cannot access a localhost backend on the server
 * but with a simple PHP call, the fetch can be replaced, however, it requires PHP

## eventual features

some features being considered:

 * loading all checkins in a local [sqlite] database and only refreshing when prompted
 * allowing checkins with a simple search bar
 * sorting by beer or brewery or location
 * simple counts of how many beers by time or place


if it becomes to complex, then why not just use the app?
