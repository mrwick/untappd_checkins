let b = document.body;
let d0 = document.createElement("div");
let dl = new URL(document.location);
let sp = dl.searchParams;
let code = sp.get('code');
let login_page = sp.has('login');
let reg_page = sp.has('reg');

let localhost_uri = 'http://localhost';
let localhost_port = ':40004';
let localhost_url = localhost_uri + localhost_port;
let checkins_url = localhost_url + '/direct_checkins';
let authenticate_url = localhost_url + '/authenticate';
let authorise_url = localhost_url + '/authorise';
let login_url = localhost_url + '/user';

let login_user = {};

let fetch_headers = {
    mode: 'cors',
    headers: {
        'Content-Type': 'application/json',
        'Origin': localhost_uri,
      }
    }

function log(text) {
    console.log('logmsg: ' + text + "\n");
}

log('searchparams: ' + sp);

function ahref(h, t) {
    // href, optional text
    if (!t) t = h;
    return '<a href=' + h +'>' + t + "</a>\n";
}

function wrapboldbr(t) {
    f = document.createDocumentFragment();
    b = document.createElement("b");
    br = document.createElement("br");
    b.innerText = t;
    f.append(b, br);

    return f;
    // "<b>" + t + "</b><br/>\n";
}

function fetch_login(login_data) {
    login_headers = fetch_headers;
    login_headers.method = "POST";
    login_headers.body = login_data;
    fetch(login_url, login_headers).
        then(response => response.json()).
        then(json => index(json))
}

function login_form() {
    form = document.createElement("form");
    // form.action = login_url;
    // form.method = 'post';

    input_login = document.createElement("input");
    input_login.name = 'username';
    input_login.value = 'username';

    input_word = document.createElement("input");
    input_word.name = 'password';
    input_word.value = 'password';

    submit = document.createElement("input");
    submit.type = 'submit';
    submit.value = 'login';
    submit.onsubmit = fetch_login;

    form.append(input_login, input_word, submit);
    // create post/get request and submit
    // then read response from server

    return form;
}

function index(json=null) {
    if json {
        d0.innerHTML = 'logged in';
    } else {
        d0.innerHTML = 'bienvenue';
        d0.innerHTML += "<br/>\n";
        d0.innerHTML += ahref('/html/untappd_checkins.html?login', 'LOGIN');
        d0.innerHTML += "<br/>\n";
        d0.innerHTML += ahref('/html/untappd_checkins.html?reg', 'REGISTER');
    }
}

function login() {
    d0.innerHTML = ahref('/html/untappd_checkins.html', 'home');
    d0.innerHTML += "<br/>\n";
    d0.innerHTML += 'login';
    d0.innerHTML += "<br/>\n";
    d0.appendChild(login_form());
}

function reg() {
    d0.innerHTML = ahref('/html/untappd_checkins.html', 'home');
    d0.innerHTML += "<br/>\n";
    d0.innerHTML += 'register';
    d0.innerHTML += "<br/>\n";
}

function checkins(checkins_json) {
    let o = checkins_json.response.checkins.items;
    log('checkins length: ' + o.length);
    for (let x = 0; x < o.length; x++) {
      let p = document.createElement('p');
      p.id = 'bier' + x;
      if (o[x].created_at) {
        p.appendChild(wrapboldbr(o[x].created_at));
      }
      p.appendChild(wrapboldbr(o[x].beer.beer_name));
      p.appendChild(wrapboldbr(o[x].brewery.brewery_name));
      if (o[x].venue.venue_name) {
        p.appendChild(wrapboldbr(o[x].venue.venue_name));
      }
      d0.append(p);
    }
}
b.append(d0);

function set_access_token(json) {
    // fetch(user_id, access_token) ??
    // 
}

// code means authenticate is has been called
// pass to authorise
if (code) {
    fetch(authorise_url, fetch_headers).
        then(response => response.json()).
        then(json => set_access_token(json));
} else if (login_page) {
    login();
} else if (reg_page) {
    reg();
} else {
    index();
}

function fetch_checkins(){
    fetch(checkins_url, fetch_headers).
        then(response => response.json()).
        then(json => checkins(json));
}
