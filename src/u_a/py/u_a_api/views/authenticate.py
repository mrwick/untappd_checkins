import requests
from flask import (
        Blueprint,
        g,
        render_template,
        url_for,
        redirect,
        )
from ..models import Creds

ok = requests.codes.ok
bp = Blueprint('authenticate', __name__)

@bp.route('/authenticate')
def authenticate():
    allcreds = Creds.query.all()
    if len(allcreds) < 1:
        return "no creds"
    ru = "{}?code=code".format(url_for('home.index'))
    return redirect(ru)

    fsu = 'freeshell.de/~mrwick/untappd_checkins'
    headers = {
            'user-agent':
            'bierwick ({})'.format(creds[0].client_id)}
    auth_url = 'https://untappd.com/oauth/authenticate'
    auth_url_params = {
            'client_id': creds[0].client_id,
            'response_type': 'code',
            'redirect_url': fsu,
            'state': 'jaoui'
            }
    auth_req = requests.get(
            auth_url,
            params=auth_url_params,
            headers=headers)
    if auth_req.status_code != ok:
        auth_req.raise_for_status()
