from flask import Blueprint, request, render_template
from ..models import Creds

bp = Blueprint('admin', __name__)

@bp.route('/admin', methods=['GET', 'POST'])
def config():
    if request.form:
        cred = Creds(
                client_id=request.form.get('client_id'),
                client_secret=request.form.get('client_secret'))
        db.session.add(cred)
        db.session.commit()
    return render_template('admin.html')
